/*
 * zegarek_main.c
 * 
 *	Microcontroller: Atmel AVR ATMega 8L
 *  	  Compiler: AVR-gcc
 * 		Created on: 24 paz 2017
 *   	    Author: Karol Witkowski
 */

//#define FOSC 8000000

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <util/delay.h>

#include "configuration.h"
#include "util/makra.h"

#include "modules/adc.h"
#include "modules/hd44780.h"
#include "modules/key_handling.h"
#include "modules/I2C.h"
#include "modules/timers.h"

#include "libs/clock.h"

volatile u16 led_soft_timer;
volatile u8 adc_soft_timer;
volatile u8 rtc_soft_timer;
volatile u16 key_handling_soft_timer;
volatile u8 adc_val;


int main(void) {
	adc_val = 4;
	// modules initialization
	LED_INIT;
	LED_TOGGLE;
	timer1_init();
	timer2_init();
	hd44780_init();
	adc_init();
	hd44780_move_cursor(0, 2);
	hd44780_str("DO NOT");
	hd44780_str(" READ");
	hd44780_send_command(HD44780_COMMAND_SHIFT_CURSOR_RIGHT);
	I2C_init();


	u8 num = 0x08;
	//set time
	I2C_write_buf(_RTC_ADDRESS, 0x03, 1, &num);
	num = 0x18;
	I2C_write_buf(_RTC_ADDRESS, 0x04, 1, &num);



	sei();
//	u8 xd = 0;
	char * tab1;
	while (1) {
		if(!led_soft_timer){
			LED_TOGGLE;
			led_soft_timer = 100;
			hd44780_move_cursor(1, 6);
			hd44780_decimal(adc_val);
			hd44780_str("  ");
//			if(xd){
//				xd = 0;
//			} else {
//				xd = 1;
//			}
		}
		if(!rtc_soft_timer){
			rtc_soft_timer = 50;
			tab1 = clock_get_time();
			hd44780_move_cursor(0, 1);
			hd44780_str(tab1);
//			if(xd){
//				xd = 0;
//			} else {
//				xd = 1;
//			}
		}
		if(!adc_soft_timer){
			adc_soft_timer = 10;
			if (adc_val < 131)
				adc_val = 131;
			u16 val = (adc_val - 130) * 255;

			val /= 125;
			hd44780_move_cursor(1, 13);
			hd44780_decimal(val);
			hd44780_str("  ");
			TIMER2_SET_DUTY_CYCLE(val);
		}
	}
}

ISR(TIMER1_COMPA_vect){
	/*
	 * If any soft timer is not zero, decrease it.
	 * The solution with "n" cost less.
	 */
	u16 n;

	n = led_soft_timer;
	if (n)
		led_soft_timer = --n;
	n = adc_soft_timer;
	if (n)
		adc_soft_timer = --n;
	n = rtc_soft_timer;
	if (n)
		rtc_soft_timer = --n;
	n = key_handling_soft_timer;
	if (n)
		key_handling_soft_timer = --n;
}

ISR(ADC_vect){
	adc_val = ADCH;
}
