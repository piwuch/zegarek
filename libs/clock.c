/*
 * clock.c
 *
 *	Microcontroller: Atmel AVR
 *   	   Compiler: AVR-gcc
 * 		 Created on: 15-01-2018
 *   	     Author: Karol Witkowski
 */
#include "clock.h"

//#include <avr/io.h>

typedef struct {
	u8 hour;
	u8 minute;
	u8 second;

} time_date;
time_date clock;

char tab[9];
char * str = tab;

void clock_read_time(){
	u8 tab[3];
	I2C_read_buf(_RTC_ADDRESS, 0x02, 3, tab);
	clock.second = (((tab[0]>>4) & (7))*10) + (tab[0] & (15));
	clock.minute = (((tab[1]>>4) & (7))*10) + (tab[1] & (15));
	clock.hour = (((tab[2]>>4) & (3))*10) + (tab[2] & (15));
}

char * clock_get_time(){

	clock_read_time();
	u8 i = 0;
	str[i++] = clock.hour/10 + '0';
	str[i++] = clock.hour%10 + '0';
	str[i++] = ':';
	str[i++] = clock.minute/10 + '0';
	str[i++] = clock.minute%10 + '0';
	str[i++] = ':';
	str[i++] = clock.second/10 + '0';
	str[i++] = clock.second%10 + '0';
	str[i] = 0;
	return str;
}
