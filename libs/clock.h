/*
 * clock.h
 *
 *	Microcontroller: Atmel AVR
 *   	   Compiler: AVR-gcc
 * 		 Created on: 15-01-2018
 *   	     Author: Karol Witkowski
 */

#ifndef CLOCK_H_
#define CLOCK_H_

#include <avr/io.h>

#include "../util/makra.h"
#include "../configuration.h"

#include "../modules/I2C.h"

void clock_read_time();
char * clock_get_time();

#endif /* CLOCK_H_ */
