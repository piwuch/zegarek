/*
 * shift_register.h
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 25 pa? 2017
 *   	    Author: Karol Witkowski
 */
#ifndef MODULES_SHIFT_REGISTER_H_
#define MODULES_SHIFT_REGISTER_H_

#include <avr/io.h>

#include "../configuration.h"
#include "../util/makra.h"

// Macro declarations
#define _SHIFT_REGISTER_SET_DATA SET(PORT(_SHIFT_REGISTER_DATA_PORT), _BV(_SHIFT_REGISTER_DATA_PIN))
#define _SHIFT_REGISTER_CLR_DATA CLR(PORT(_SHIFT_REGISTER_DATA_PORT), _BV(_SHIFT_REGISTER_DATA_PIN))
#define _SHIFT_REGISTER_SET_CLK SET(PORT(_SHIFT_REGISTER_CLK_PORT), _BV(_SHIFT_REGISTER_CLK_PIN))
#define _SHIFT_REGISTER_CLR_CLK CLR(PORT(_SHIFT_REGISTER_CLK_PORT), _BV(_SHIFT_REGISTER_CLK_PIN))

// Function declarations
void shift_register_init();
void shift_register_send(u8 data);

#endif /* MODULES_SHIFT_REGISTER_H_ */
