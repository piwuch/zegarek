/*
 * adc.h
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 25 pa? 2017
 *   	    Author: Karol Witkowski
 */
#ifndef MODULES_ADC_H_
#define MODULES_ADC_H_

#include <avr/io.h>

#include "../configuration.h"
#include "../util/makra.h"

// Function declarations
void adc_init();
u8 adc_read_u8();

#endif /* MODULES_ADC_H_ */
