/*
 * shift_register.c
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 25 pa? 2017
 *   	    Author: Karol Witkowski
 */

#include "shift_register.h"

void shift_register_init(){
	// Set data and clock pins as output
	SET(DDR(_SHIFT_REGISTER_DATA_PORT), _BV(_SHIFT_REGISTER_DATA_PIN));
	SET(DDR(_SHIFT_REGISTER_CLK_PORT), _BV(_SHIFT_REGISTER_CLK_PIN));
	shift_register_send(0b10100101);
}

void shift_register_send(u8 data){
	for(u8 i = 0; i < 8; i++){
		if (data & 1)
			_SHIFT_REGISTER_SET_DATA;
		else
			_SHIFT_REGISTER_CLR_DATA;
		data >>= 1;
		_SHIFT_REGISTER_SET_CLK;
		_SHIFT_REGISTER_CLR_CLK;
	}
}
