/*
 * timers.c
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 26 pa? 2017
 *   	    Author: Karol Witkowski
 */

#include "timers.h"

void timer1_init(){
#ifdef TIMER1_COMPARE_MATCH_OUTPUT_MODE
	TIMER_INTERRUPT_MASK_REGISTER |= _BV(4); // interrupt on compare match
	TIMER1_CONTROL_REGISTER_B |= _BV(WGM12); // set mode of operation to CTC
#endif
	TIMER1_CONTROL_REGISTER_B |= TIMER1_PRESCALER; // set prescaler
	TIMER1_OUTPUT_COMPARE_REGISTER_A |= TIMER1_COUNTER; // set counter range
}


void timer2_init(){
#ifdef TIMER2_COMPARE_MATCH_OUTPUT_MODE
#endif
#ifdef _TIMER2_FAST_PWM_MODE
	TIMER2_CONTROL_REGISTER |= _BV(WGM21); // fast PWM
	TIMER2_CONTROL_REGISTER |= _BV(WGM20); // fast PWM
	TIMER2_CONTROL_REGISTER |= _BV(COM21); // Clear OC2 on compare match
//	TIMER2_CONTROL_REGISTER |= _BV(COM20); // Clear OC2 on compare match
#endif
	TIMER2_CONTROL_REGISTER |= TIMER2_PRESCALER; // set prescaler
	DDR(TIMER_OC2_PORT) |= _BV(TIMER_OC2_PIN);
}

