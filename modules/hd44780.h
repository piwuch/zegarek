/*
 * hd44780.h
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 17 lis 2017
 *   	    Author: Karol Witkowski
 */
#ifndef MODULES_HD44780_H_
#define MODULES_HD44780_H_

#include <avr/io.h>

#include "../configuration.h"
#include "../util/makra.h"

//  Commands
#define HD44780_COMMAND_CLEAR_SCREEN			1
#define HD44780_COMMAND_HOME 					2
#define HD44780_COMMAND_MODE_SHIFT_CURSOR_LEFT	4
#define HD44780_COMMAND_MODE_SHIFT_TEXT_RIGHT	5
#define HD44780_COMMAND_MODE_SHIFT_CURSOR_RIGHT	6
#define HD44780_COMMAND_MODE_SHIFT_TEXT_LEFT	7
#define HD44780_COMMAND_SHIFT_TEXT_RIGHT		6
#define HD44780_COMMAND_DISPLAY_OFF				8
#define HD44780_COMMAND_CURSOR_OFF				12 // turns diplay on
#define HD44780_COMMAND_CURSOR_BLINK			13 // turns diplay on
#define HD44780_COMMAND_CURSOR_SHOW				14 // turns diplay on
#define HD44780_COMMAND_SHIFT_CURSOR_LEFT 		16
#define HD44780_COMMAND_SHIFT_CURSOR_RIGHT 		20
#define HD44780_COMMAND_SHIFT_ALL_LEFT 			24
#define HD44780_COMMAND_SHIFT_ALL_RIGHT 		28
#define HD44780_COMMAND_4_BIT_1_LINE 	 		32
#define HD44780_COMMAND_4_BIT_2_LINES 	 		40
#define HD44780_COMMAND_8_BIT_1_LINE  			48
#define HD44780_COMMAND_8_BIT_2_LINES  			56
#define HD44780_COMMAND_MOVE_CURSOR  			128

//  Function declarations
void hd44780_init(void);
void hd44780_str(char * str);
void hd44780_move_cursor(u8 y, u8 x);
void hd44780_send_char(char c);
void hd44780_flash_string(char * str);
void hd44780_eeprom_string(char * str);
void hd44780_decimal(u16 val);
void hd44780_hex(u16 val);
void hd44780_user_char(u8 nr, u8 *char_data);
void hd44780_user_flash_char(u8 nr, u8 *char_data);
void hd44780_user_eeprom_char(u8 nr, u8 *char_data);
void hd44780_send_command(u8 cmd);

#endif /* MODULES_HD44780_H_ */
