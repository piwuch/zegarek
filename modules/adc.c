/*
 * adc.c
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 25 pa? 2017
 *   	    Author: Karol Witkowski
 */

#include "adc.h"

void adc_init(){
#ifdef _ADC_LEFT_ADJUST_RESULT
	SET(ADMUX, _BV(ADLAR));
#endif
	SET(ADMUX, _ADC_STARTING_CHANNEL);
#ifdef _ADC_FREE_RUNNING_MODE
	SET(ADCSRA, _BV(5)); // ADFR
#endif
	SET(ADCSRA, _ADC_PRESCALER);
	SET(ADCSRA, _BV(ADIE)); // interrupt enable
	SET(ADCSRA, _BV(ADEN)); // finally enable ADC
	SET(ADCSRA, _BV(ADSC)); // start measure
}

#ifdef _ADC_LEFT_ADJUST_RESULT
u8 adc_read_u8(){
	u8 result;
	result = ADC;
	return result;
}
#endif
