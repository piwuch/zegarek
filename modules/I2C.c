/*
 * I2C.c
 *
 *	Microcontroller: Atmel AVR
 *   	   Compiler: AVR-gcc
 * 		 Created on: 03-02-2015
 *   	     Author: Karol Witkowski
 */
#include <avr/io.h>

#include "I2C.h"

#ifdef _I2C_USE_FIXED_BITRATE
void I2C_init() {
	u8 bitrate_div;

	bitrate_div = ((F_CPU/1000l)/_I2C_STARTING_BITRATE_KHZ);
	if(bitrate_div >= 16)
		bitrate_div = (bitrate_div-16)/2;

	_I2C_BITRATE_REGISTER = bitrate_div;
}
#else // _I2C_USE_FIXED_BITRATE
void I2C_init(uint16_t bitrateKHz) {
	u8 bitrate_div;

	bitrate_div = ((F_CPU/1000l)/bitrateKHz);
	if(bitrate_div >= 16)
		bitrate_div = (bitrate_div-16)/2;

	_I2C_BITRATE_REGISTER = bitrate_div;
}
#endif // _I2C_USE_FIXED_BITRATE

void _I2C_start(void) {
	_I2C_CONTROL_REGISTER = _BV(_I2C_INTERRUPT_FLAG)|_BV(_I2C_ENABLE_BIT)|_BV(_I2C_START_CONDITION_BIT);
	while (!GET(_I2C_CONTROL_REGISTER, _I2C_INTERRUPT_FLAG));
}

void _I2C_stop(void) {
	_I2C_CONTROL_REGISTER = _BV(_I2C_INTERRUPT_FLAG)|_BV(_I2C_ENABLE_BIT)|_BV(_I2C_STOP_CONDITION_BIT);
	while (!GET(_I2C_CONTROL_REGISTER, _I2C_STOP_CONDITION_BIT));
}

void _I2C_write(u8 byte) {
	_I2C_DATA_REGISTER = byte;
	_I2C_CONTROL_REGISTER = _BV(_I2C_INTERRUPT_FLAG)|_BV(_I2C_ENABLE_BIT);
	while (!GET(_I2C_CONTROL_REGISTER, _I2C_INTERRUPT_FLAG));
}

u8 _I2C_read(u8 ack) {
	_I2C_CONTROL_REGISTER = _BV(_I2C_INTERRUPT_FLAG)|(ack<<TWEA)|_BV(_I2C_ENABLE_BIT);
	while (!GET(_I2C_CONTROL_REGISTER, _I2C_INTERRUPT_FLAG));
	return _I2C_DATA_REGISTER;
}



void I2C_write_buf( u8 SLA, u8 adr, u8 len, u8 *buf ) {
	_I2C_start();
	_I2C_write(SLA);
	_I2C_write(adr);
	while (len--) _I2C_write(*buf++);
	_I2C_stop();
}



void I2C_read_buf(u8 SLA, u8 adr, u8 len, u8 *buf) {
	_I2C_start();
	_I2C_write(SLA);
	_I2C_write(adr);
	_I2C_start();
	_I2C_write(SLA + 1);
	while (len--) *buf++ = _I2C_read( len ? 1 : 0 );
	_I2C_stop();
}
