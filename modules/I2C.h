/*
 * I2C.h
 *
 *	Microcontroller: Atmel AVR
 *   	   Compiler: AVR-gcc
 * 		 Created on: 03-02-2015
 *   	     Author: Karol Witkowski
 */

#ifndef I2C_H_
#define I2C_H_

#include "../util/makra.h"
#include "../configuration.h"

#define _I2C_CONTROL_REGISTER 		TWCR
#define _I2C_INTERRUPT_FLAG 			TWINT
#define _I2C_ENABLE_ACKNOWLEDGE_BIT 	TWEA
#define _I2C_START_CONDITION_BIT 		TWSTA
#define _I2C_STOP_CONDITION_BIT 		TWSTO
#define _I2C_WRITE_COLLISION_FLAG 		TWWC
#define _I2C_ENABLE_BIT 				TWEN
#define _I2C_INTERRUPT_ENABLE 			TWIE

#define _I2C_BITRATE_REGISTER 		TWBR
#define _I2C_DATA_REGISTER 			TWDR

// funkcje
void _I2C_start(void);
void _I2C_stop(void);
void _I2C_write(u8 byte);
u8 _I2C_read(u8 ack);

#ifdef _I2C_USE_FIXED_BITRATE
void I2C_init();
#else // _I2C_USE_FIXED_BITRATE
void I2C_init(uint16_t bitrateKHz);
#endif // _I2C_USE_FIXED_BITRATE

void I2C_write_buf( u8 SLA, u8 adr, u8 len, u8 *buf );
void I2C_read_buf(u8 SLA, u8 adr, u8 len, u8 *buf);

#endif /* I2C_H_ */
