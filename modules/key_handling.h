/*
 * key_handling.h
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 2 lis 2017
 *   	    Author: Karol Witkowski
 */
#ifndef MODULES_KEY_HANDLING_H_
#define MODULES_KEY_HANDLING_H_

#include <avr/io.h>

#include "../configuration.h"
#include "../util/makra.h"
//extern volatile u16 key_handling_soft_timer;

// Key masks
#define KEY1_MASK 	_BV(_KEY1_PIN)
#define KEY1_PIN 	PIN(_KEY1_PORT)
#define KEY2_MASK 	_BV(_KEY2_PIN)
#define KEY2_PIN 	PIN(_KEY2_PORT)
#define KEY3_MASK 	_BV(_KEY3_PIN)
#define KEY3_PIN 	PIN(_KEY3_PORT)
#define KEY4_MASK 	_BV(_KEY4_PIN)
#define KEY4_PIN 	PIN(_KEY4_PORT)
#define KEY5_MASK 	_BV(_KEY5_PIN)
#define KEY5_PIN 	PIN(_KEY5_PORT)
#if _NUMBER_OF_KEYS > 5
#error Only 5 keys are handled here!
#endif

// Function declarations
void key_handling_init();
#ifdef KEY_HANDLING_FIXED_REPEAT_TIME
void key_handle(u8 * key_state, volatile u8 *KPIN, u8 key_mask,
		void (*push_proc)(void), void (*rep_proc)(void) );
#else
void key_handle(u8 * key_state, volatile u8 *KPIN,
		u8 key_mask, u16 rep_time, u16 rep_wait,
		void (*push_proc)(void), void (*rep_proc)(void) );
#endif /* KEY_HANDLING_FIXED_REPEAT_TIME */

#endif /* MODULES_KEY_HANDLING_H_ */
