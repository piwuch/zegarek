/*
 * key_handling.c
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 2 lis 2017
 *   	    Author: Karol Witkowski
 */

#include "key_handling.h"

extern volatile u16 key_handling_soft_timer;

void key_handling_init(){
	PORT(_KEY1_PORT) |= KEY1_MASK;
#if _NUMBER_OF_KEYS > 1
	PORT(_KEY2_PORT) |= KEY2_MASK;
#endif
#if _NUMBER_OF_KEYS > 2
	PORT(_KEY3_PORT) |= KEY3_MASK;
#endif
#if _NUMBER_OF_KEYS > 3
	PORT(_KEY4_PORT) |= KEY4_MASK;
#endif
#if _NUMBER_OF_KEYS > 4
	PORT(_KEY5_PORT) |= KEY5_MASK;
#endif
#if _NUMBER_OF_KEYS > 5
#error Only 5 keys are handled here!
#endif
}


#ifdef KEY_HANDLING_FIXED_REPEAT_TIME
void key_handle(u8 * key_state, volatile u8 *KPIN, u8 key_mask,
		void (*push_proc)(void), void (*rep_proc)(void) ){
	u16 rep_time = KEY_HANDLING_REPEAT_INTERVAL;
	u16 rep_wait = KEY_HANDLING_WAIT_FOR_REPEAT;
#else
void key_handle(u8 * key_state, volatile u8 *KPIN,
		u8 key_mask, u16 rep_time, u16 rep_wait,
		void (*push_proc)(void), void (*rep_proc)(void) ){
#ifdef KEY_HANDLING_DEFAULT_REPEAT_TIME
	if(!rep_time) rep_time = KEY_HANDLING_REPEAT_INTERVAL;
	if(!rep_wait) rep_time = KEY_HANDLING_WAIT_FOR_REPEAT;
#endif /* KEY_HANDLING_DEFAULT_REPEAT_TIME */
#endif /* KEY_HANDLING_FIXED_REPEAT_TIME */
	enum {idle, debounce, go_rep, wait_rep, rep};

	u8 key_down = !(*KPIN & key_mask);

	if( key_down && *key_state==idle ) {
		*key_state = debounce;
		key_handling_soft_timer = 5;
	} else
	if( *key_state  ) {
		if( key_down && debounce==*key_state && !key_handling_soft_timer ) {
			*key_state = go_rep;
			key_handling_soft_timer=15;
		} else
		if( !key_down && *key_state>debounce && *key_state<rep ) {
			if(push_proc) push_proc();						/* KEY_UP */
			*key_state=idle;
		} else
		if( key_down && go_rep==*key_state && !key_handling_soft_timer ) {
			*key_state = wait_rep;
			key_handling_soft_timer=rep_wait;
		} else
		if( key_down && wait_rep==*key_state && !key_handling_soft_timer ) {
			*key_state = rep;
		} else
		if( key_down && rep==*key_state && !key_handling_soft_timer ) {
			key_handling_soft_timer = rep_time;
			if(rep_proc) rep_proc();
		}
	}
	if( *key_state>=3 && !key_down ) *key_state = idle;
}
