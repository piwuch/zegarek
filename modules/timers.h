/*
 * timers.h
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 26 pa? 2017
 *   	    Author: Karol Witkowski
 */

#ifndef MODULES_TIMERS_H_
#define MODULES_TIMERS_H_

#include <avr/io.h>

#include "../util/makra.h"
#include "../configuration.h"
// Pinout
#define TIMER_OC2_PORT	B
#define TIMER_OC2_PIN 	PB3

// Registers
#ifndef TIMER_INTERRUPT_MASK_REGISTER
#define TIMER_INTERRUPT_MASK_REGISTER TIMSK
#endif /* TIMER_INTERRUPT_MASK_REGISTER */
#define TIMER1_CONTROL_REGISTER_A TCCR1A
#define TIMER1_CONTROL_REGISTER_B TCCR1B
#define TIMER1_OUTPUT_COMPARE_REGISTER_A OCR1A
#define TIMER1_OUTPUT_COMPARE_REGISTER_B OCR1B

#define TIMER2_CONTROL_REGISTER TCCR2
#define TIMER2_OUTPUT_COMPARE_REGISTER OCR2

// Macro definitions
#define TIMER2_SET_DUTY_CYCLE(duty_cycle) TIMER2_OUTPUT_COMPARE_REGISTER = (duty_cycle)

// Function declarations
void timer1_init();
void timer2_init();

#endif /* MODULES_TIMERS_H_ */

