/*
 * hd44780.c
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 17 lis 2017
 *   	    Author: Karol Witkowski
 */
#define FOSC 8000000

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <util/delay.h>

#include "hd44780.h"

#define _HD44780_LINE2 64

#define _HD44780_SET_E SET(PORT(_HD44780_E_PORT), _BV(_HD44780_E_PIN))
#define _HD44780_CLR_E CLR(PORT(_HD44780_E_PORT), _BV(_HD44780_E_PIN))
#define _HD44780_SET_RS SET(PORT(_HD44780_RS_PORT), _BV(_HD44780_RS_PIN))
#define _HD44780_CLR_RS CLR(PORT(_HD44780_RS_PORT), _BV(_HD44780_RS_PIN))
#define _HD44780_SET_RW SET(PORT(_HD44780_RW_PORT), _BV(_HD44780_RW_PIN))
#define _HD44780_CLR_RW CLR(PORT(_HD44780_RW_PORT), _BV(_HD44780_RW_PIN))


u8 check_BF(void);

static inline void _hd44780_data_dir_out(void){
	DDR(_HD44780_D7_PORT)	|= (1<<_HD44780_D7_PIN);
	DDR(_HD44780_D6_PORT)	|= (1<<_HD44780_D6_PIN);
	DDR(_HD44780_D5_PORT)	|= (1<<_HD44780_D5_PIN);
	DDR(_HD44780_D4_PORT)	|= (1<<_HD44780_D4_PIN);
}

static inline void data_dir_in(void){
	DDR(_HD44780_D7_PORT)	&= ~(1<<_HD44780_D7_PIN);
	DDR(_HD44780_D6_PORT)	&= ~(1<<_HD44780_D6_PIN);
	DDR(_HD44780_D5_PORT)	&= ~(1<<_HD44780_D5_PIN);
	DDR(_HD44780_D4_PORT)	&= ~(1<<_HD44780_D4_PIN);
}

static inline void hd44780_send_halfbyte(u8 data){
	if (data&(1<<0)) PORT(_HD44780_D4_PORT) |= (1<<_HD44780_D4_PIN); else PORT(_HD44780_D4_PORT) &= ~(1<<_HD44780_D4_PIN);
	if (data&(1<<1)) PORT(_HD44780_D5_PORT) |= (1<<_HD44780_D5_PIN); else PORT(_HD44780_D5_PORT) &= ~(1<<_HD44780_D5_PIN);
	if (data&(1<<2)) PORT(_HD44780_D6_PORT) |= (1<<_HD44780_D6_PIN); else PORT(_HD44780_D6_PORT) &= ~(1<<_HD44780_D6_PIN);
	if (data&(1<<3)) PORT(_HD44780_D7_PORT) |= (1<<_HD44780_D7_PIN); else PORT(_HD44780_D7_PORT) &= ~(1<<_HD44780_D7_PIN);
}

static inline u8 hd44780_read_halfbyte(void){
	u8 result=0;

	if(PIN(_HD44780_D4_PORT)&(1<<_HD44780_D4_PIN)) result |= (1<<0);
	if(PIN(_HD44780_D5_PORT)&(1<<_HD44780_D5_PIN)) result |= (1<<1);
	if(PIN(_HD44780_D6_PORT)&(1<<_HD44780_D6_PIN)) result |= (1<<2);
	if(PIN(_HD44780_D7_PORT)&(1<<_HD44780_D7_PIN)) result |= (1<<3);

	return result;
}

void _hd44780_write_byte(unsigned char _data){
	_hd44780_data_dir_out();
	_HD44780_CLR_RW;

	_HD44780_SET_E;
	hd44780_send_halfbyte(_data >> 4);
	_HD44780_CLR_E;

	_HD44780_SET_E;
	hd44780_send_halfbyte(_data);
	_HD44780_CLR_E;

	while( (check_BF() & (1<<7)) );
}

u8 _hd44780_read_byte(void){
	u8 result=0;
	data_dir_in();

	_HD44780_SET_RW;

	_HD44780_SET_E;
	result = (hd44780_read_halfbyte() << 4);
	_HD44780_CLR_E;
	_HD44780_SET_E;
	result |= hd44780_read_halfbyte();
	_HD44780_CLR_E;

	return result;
}


u8 check_BF(void){
	_HD44780_CLR_RS;
	return _hd44780_read_byte();
}


void hd44780_send_command(u8 cmd){
	_HD44780_CLR_RS;
	_hd44780_write_byte(cmd);
}

void hd44780_write_data(u8 data){
	_HD44780_SET_RS;
	_hd44780_write_byte(data);
}


#ifdef _HD44780_USE_SEND_CHAR
void hd44780_send_char(char c){
	hd44780_write_data( ( c>=0x80 && c<=0x87 ) ? (c & 0x07) : c);
}
#endif

void hd44780_str(char * str){
	register char znak;
	while ( (znak=*(str++)) )
		hd44780_write_data( ( znak>=0x80 && znak<=0x87 ) ? (znak & 0x07) : znak);
}

#ifdef _HD44780_USE_FLASH_MEM_STRING
void hd44780_flash_string(char * str){
	register char znak;
	while ( (znak=pgm_read_byte(str++)) )
		hd44780_write_data( ( (znak>=0x80) && (znak<=0x87) ) ? (znak & 0x07) : znak);
}
#endif


#ifdef _HD44780_USE_EEPROM_STRING
void hd44780_eeprom_string(char * str){
	register char znak;
	while(1){
		znak=eeprom_read_byte( (u8 *)(str++) );
		if(!znak || znak==0xFF) break;
		else hd44780_write_data( ( (znak>=0x80) && (znak<=0x87) ) ? (znak & 0x07) : znak);
	}
}
#endif


#ifdef _HD44780_USE_SEND_INT
void hd44780_decimal(u16 val){
	char bufor[7];
	hd44780_str( itoa(val, bufor, 10) );
}
#endif

#ifdef _HD44780_USE_SEND_HEX
void hd44780_hex(u16 val){
	char bufor[5];
	hd44780_str( itoa(val, bufor, 16) );
}
#endif

#ifdef _HD44780_USE_USER_CHAR
//----------------------------------------------------------------------------------------
//		Define your own char
//
//		nr: 		- char nr from 0 to 7
//		*char_data:	- 7 bytes - 7 lines of char
//----------------------------------------------------------------------------------------
void hd44780_user_char(u8 nr, u8 *char_data){
	register u8 i,c;
	hd44780_send_command( 64+((nr&0x07)*8) );
	for(i=0;i<8;i++){
		c = *(char_data++);
		hd44780_write_data(c);
	}
}
#endif

#ifdef _HD44780_USE_FLASH_USER_CHAR
void hd44780_user_flash_char(u8 nr, u8 *char_data){
	register u8 i,c;
	hd44780_send_command( 64+((nr&0x07)*8) );
	for(i=0;i<8;i++){
		c = pgm_read_byte(char_data++);
		hd44780_write_data(c);
	}
}
#endif

#ifdef _HD44780_USE_EEPROM_USER_CHAR
void hd44780_user_eeprom_char(u8 nr, u8 *char_data){
	register u8 i,c;
	hd44780_send_command( 64+((nr&0x07)*8) );
	for(i=0;i<8;i++){
		c = eeprom_read_byte(char_data++);
		hd44780_write_data(c);
	}
}
#endif


#ifdef _HD44780_USE_MOVE_CURSOR
void hd44780_move_cursor(u8 y, u8 x){
	switch(y){
		case 1:{
			y = _HD44780_LINE2;
			break;
		}
	    default: {
	    	y = 0; break;
	    }
	}
	hd44780_send_command( (HD44780_COMMAND_MOVE_CURSOR + y + x) );
}
#endif


//----------------------------------------------------------------------------------------
//
//		 ******* INICJALIZACJA WYŚWIETLACZA LCD ********
//
//----------------------------------------------------------------------------------------
void hd44780_init(void){
	_hd44780_data_dir_out();
	DDR(_HD44780_RS_PORT) |= (1<<_HD44780_RS_PIN);
	DDR(_HD44780_E_PORT) |= (1<<_HD44780_E_PIN);
	DDR(_HD44780_RW_PORT) |= (1<<_HD44780_RW_PIN);

	PORT(_HD44780_RS_PORT) |= (1<<_HD44780_RS_PIN);
	PORT(_HD44780_E_PORT) |= (1<<_HD44780_E_PIN);
	PORT(_HD44780_RW_PORT) |= (1<<_HD44780_RW_PIN);

	_delay_ms(15);
	PORT(_HD44780_E_PORT) &= ~(1<<_HD44780_E_PIN);
	PORT(_HD44780_RS_PORT) &= ~(1<<_HD44780_RS_PIN);
	PORT(_HD44780_RW_PORT) &= ~(1<<_HD44780_RW_PIN);

	_HD44780_SET_E;
	hd44780_send_halfbyte(0x03);
	_HD44780_CLR_E;
	_delay_ms(4.1);

	_HD44780_SET_E;
	hd44780_send_halfbyte(0x03);
	_HD44780_CLR_E;
	_delay_us(100);

	_HD44780_SET_E;
	hd44780_send_halfbyte(0x03);
	_HD44780_CLR_E;
	_delay_us(100);

	_HD44780_SET_E;
	hd44780_send_halfbyte(0x02);
	_HD44780_CLR_E;
	_delay_us(100);

	hd44780_send_command( HD44780_COMMAND_4_BIT_2_LINES );
	hd44780_send_command( HD44780_COMMAND_DISPLAY_OFF );
	hd44780_send_command( HD44780_COMMAND_CURSOR_OFF );
	hd44780_send_command( HD44780_COMMAND_SHIFT_TEXT_RIGHT );
	hd44780_send_command( HD44780_COMMAND_CLEAR_SCREEN );
}
