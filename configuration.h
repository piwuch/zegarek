/*
 * configuration.h
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 25 pa? 2017
 *   	    Author: Karol Witkowski
 */
#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_

#include "util/makra.h"
/*
 * This file is for storing basic configuration of every module used.
 * Information stored: ports, pins, operation modes.
 * Modules used:
 * 		ADC
 * 		HD44780
 * 		I2C
 * 		Key Handling,
 * 		LED Handling macros,
 * 		RTC 8563P
 * 		Shift Register,
 * 		Timers.
 */


/*
 * ADC
 */
#define _ADC_STARTING_CHANNEL 0 // 0 - 5

// if defined result is on higher bits of ADCH:ADCL
#define _ADC_LEFT_ADJUST_RESULT

// if defined ADC updates the Data Registers continuously
#define _ADC_FREE_RUNNING_MODE

//  Prescaler configurations:
//#define _ADC_PRESCALER  		0 // CLK/2
//#define _ADC_PRESCALER 		1 // CLK/2
//#define _ADC_PRESCALER 		2 // CLK/4
//#define _ADC__PRESCALER 		3 // CLK/8
//#define _ADC_PRESCALER 		4 // CLK/16
//#define _ADC_PRESCALER 		5 // CLK/32
//#define _ADC_PRESCALER 		6 // CLK/64
#define _ADC_PRESCALER 		7 // CLK/128
/*
 * HD44780 Configuration
 */
//  Pinout
#define _HD44780_D7_PORT 	D
#define _HD44780_D7_PIN 	5
#define _HD44780_D6_PORT 	D
#define _HD44780_D6_PIN 	6
#define _HD44780_D5_PORT 	D
#define _HD44780_D5_PIN 	7
#define _HD44780_D4_PORT 	B
#define _HD44780_D4_PIN 	0

#define _HD44780_RS_PORT 	B
#define _HD44780_RS_PIN 	7
#define _HD44780_RW_PORT 	B
#define _HD44780_RW_PIN 	6
#define _HD44780_E_PORT 	D
#define _HD44780_E_PIN 		4

//  Compilation Settings - comment to save resources
#define _HD44780_USE_MOVE_CURSOR 			// 18 bytes
//#define _HD44780_USE_FLASH_MEM_STRING 	// 42 bytes
//#define _HD44780_USE_EEPROM_STRING 		// 54 bytes
//#define _HD44780_USE_SEND_CHAR 			// 12 bytes
#define _HD44780_USE_SEND_INT 			// 186 bytes
//#define _HD44780_USE_SEND_HEX 			// 186 bytes
//#define _HD44780_USE_USER_CHAR 			// 48 bytes
//#define _HD44780_USE_FLASH_USER_CHAR 		// 52 bytes
//#define _HD44780_USE_EEPROM_USER_CHAR 	// 84 bytes

/*
 * I2C Configuration
 */
#define _I2C_USE_FIXED_BITRATE

#define _I2C_STARTING_BITRATE_KHZ 100

#define _RTC_ADDRESS 0b10100010

/*
 * Key Handling Configuration
 */
#define _NUMBER_OF_KEYS 5

//  Pinout
#define _KEY1_PIN 	PD2
#define _KEY1_PORT 	D
#define _KEY2_PIN 	PD1
#define _KEY2_PORT 	D
#define _KEY3_PIN 	PD0
#define _KEY3_PORT 	D
#define _KEY4_PIN 	PD0
#define _KEY4_PORT 	D
#define _KEY5_PIN 	PD0
#define _KEY5_PORT 	D

// comment this line to provide repeat time as parameter
#define KEY_HANDLING_FIXED_REPEAT_TIME
#ifndef KEY_HANDLING_FIXED_REPEAT_TIME
// comment this line to turn off default setting repeat time
#define KEY_HANDLING_DEFAULT_REPEAT_TIME
#endif /* KEY_HANDLING_FIXED_REPEAT_TIME */

#if defined(KEY_HANDLING_FIXED_REPEAT_TIME) || defined(KEY_HANDLING_DEFAULT_REPEAT_TIME)
#define KEY_HANDLING_WAIT_FOR_REPEAT 60
#define KEY_HANDLING_REPEAT_INTERVAL 50
#endif /* KEY_HANDLING_FIXED_REPEAT_TIME || KEY_HANDLING_DEFAULT_REPEAT_TIME */

/*
 * LED handling - simple macros
 */
//  Pinout
#define _LED_PIN 	PD0
#define _LED_PORT 	D

#define _LED_MASK _BV(_LED_PIN)

#define LED_INIT 	SET(DDR(_LED_PORT), _LED_MASK)
#define LED_TOGGLE 	TOG(PORT(_LED_PORT), _LED_MASK)
#define LED_SET 	SET(PORT(_LED_PORT), _LED_MASK)
#define LED_CLEAR 	CLR(PORT(_LED_PORT), _LED_MASK)

/*
 * Shift Register Configuration
 */
//  Pinout
#define _SHIFT_REGISTER_DATA_PIN	PC3
#define _SHIFT_REGISTER_DATA_PORT 	C
#define _SHIFT_REGISTER_CLK_PIN		PC2
#define _SHIFT_REGISTER_CLK_PORT 	C


/*
 * Timer 1 Configuration
 */
#define TIMER1_COMPARE_MATCH_OUTPUT_MODE
//  Prescaler configurations:
//#define TIMER1_PRESCALER  								0 // No clock source. (Timer/Counter stopped)
//#define TIMER1_PRESCALER 		1 // CLK/1
//#define TIMER1_PRESCALER 		2 // CLK/8
#define TIMER1_PRESCALER 		3 // CLK/64
//#define TIMER1_PRESCALER 		4 // CLK/256
//#define TIMER1_PRESCALER 		5 // CLK/1024
//#define TIMER1_PRESCALER 		6 // External clock source on T1 pin. Clock on falling edge
//#define TIMER1_PRESCALER 		7 // External clock source on T1 pin. Clock on rising edge

//  16-bit value (1 - 65535)
#define TIMER1_COUNTER 			1250

/*
 * Timer 2 Configuration
 */
//modes of operation
//#define TIMER2_COMPARE_MATCH_OUTPUT_MODE
#define _TIMER2_FAST_PWM_MODE
//  Prescaler configurations:
//#define TIMER2_PRESCALER  	0 // No clock source. (Timer/Counter stopped)
#define TIMER2_PRESCALER 		1 // CLK/1
//#define TIMER2_PRESCALER 		2 // CLK/8
//#define TIMER2_PRESCALER 		3 // CLK/32
//#define TIMER2_PRESCALER 		4 // CLK/64
//#define TIMER2_PRESCALER 		5 // CLK/128
//#define TIMER2_PRESCALER 		6 // CLK/256
//#define TIMER2_PRESCALER 		7 // CLK/1024

//  16-bit value (1 - 65535)
#define TIMER2_COUNTER 			1250

#endif /* CONFIGURATION_H_ */
